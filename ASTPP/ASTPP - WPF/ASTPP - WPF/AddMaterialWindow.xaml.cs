﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ASTPP___WPF.Control;

namespace ASTPP___WPF
{
    /// <summary>
    /// Логика взаимодействия для AddMaterialWindow.xaml
    /// </summary>
    public partial class AddMaterialWindow : Window
    {
        private int deviceModelId;
        private int processId;
        private int operationId;
        private int materialId;
        
        private string deviceModelName;
        private string processName;
        private string operationName;
        private string materialName;

        private int materialQuantity;

        public AddMaterialWindow()
        {
            InitializeComponent();
            dataGridDevice.ItemsSource = DeviceTransactionScript.ShowAllDeviceModelsWithID();
            dataGridMaterial.ItemsSource = MaterialTransactionScript.ShowAll();
        }

        private void showSelection()
        {
            currentSelectionTextBlock.Text = "DeviceModelName: " + deviceModelName + "\nProcessName: " + processName + "\nOperationName: " + operationName + "\nMaterialName: " + materialName;
        }
        private void dataGridDevice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dataGridDevice.SelectedItems.Count == 1)
            {
                TextBlock contentId = dataGridDevice.Columns[0].GetCellContent(dataGridDevice.SelectedItem) as TextBlock;
                TextBlock contentName = dataGridDevice.Columns[1].GetCellContent(dataGridDevice.SelectedItem) as TextBlock;
                if (contentId != null)
                {
                    deviceModelId = Convert.ToInt32(contentId.Text);
                    deviceModelName = contentName.Text;
                    dataGridProcess.ItemsSource = ProcessTransactionScript.ShowProcessesBySelectedDevice(deviceModelName);
                }
                showSelection();
            }
        }

        private void dataGridProcess_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dataGridProcess.SelectedItems.Count == 1)
            {
                TextBlock contentId = dataGridProcess.Columns[0].GetCellContent(dataGridProcess.SelectedItem) as TextBlock;
                TextBlock contentName = dataGridProcess.Columns[1].GetCellContent(dataGridProcess.SelectedItem) as TextBlock;
                if (contentId != null)
                {
                    processId = Convert.ToInt32(contentId.Text);
                    processName = contentName.Text;
                    dataGridOperation.ItemsSource = OperationTransactionScript.ShowOperationsByProcessID(processId, deviceModelId);
                }
                showSelection();
            }
        }

        private void dataGridOperation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dataGridProcess.SelectedItems.Count == 1)
            {
                TextBlock contentId = dataGridOperation.Columns[0].GetCellContent(dataGridOperation.SelectedItem) as TextBlock;
                TextBlock contentName = dataGridOperation.Columns[2].GetCellContent(dataGridOperation.SelectedItem) as TextBlock;
                if (contentId != null)
                {
                    operationId = Convert.ToInt32(contentId.Text);
                    operationName = contentName.Text;
                }
                showSelection();
            }
        }

        private void dataGridMaterial_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dataGridProcess.SelectedItems.Count == 1)
            {
                TextBlock contentId = dataGridMaterial.Columns[0].GetCellContent(dataGridMaterial.SelectedItem) as TextBlock;
                TextBlock contentName = dataGridMaterial.Columns[1].GetCellContent(dataGridMaterial.SelectedItem) as TextBlock;
                if (contentId != null)
                {
                    materialId = Convert.ToInt32(contentId.Text);
                    materialName = contentName.Text;
                }
                showSelection();
            }
        }

        private void comboBoxFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBoxItemFilterByParts.IsSelected)
            {
                TextBlock content = comboBoxItemFilterByParts.Content as TextBlock;
                dataGridMaterial.ItemsSource = MaterialTransactionScript.ShowMaterialBySelectedMaterialType(content.Text);
            }
            if (comboBoxItemFilterByEquipment.IsSelected)
            {
                TextBlock content = comboBoxItemFilterByEquipment.Content as TextBlock;
                dataGridMaterial.ItemsSource = MaterialTransactionScript.ShowMaterialBySelectedMaterialType(content.Text);
            }
            if (comboBoxItemFilterByTools.IsSelected)
            {
                TextBlock content = comboBoxItemFilterByTools.Content as TextBlock;
                dataGridMaterial.ItemsSource = MaterialTransactionScript.ShowMaterialBySelectedMaterialType(content.Text);
            }
            if (comboBoxItemFilterByConsumables.IsSelected)
            {
                TextBlock content = comboBoxItemFilterByConsumables.Content as TextBlock;
                dataGridMaterial.ItemsSource = MaterialTransactionScript.ShowMaterialBySelectedMaterialType(content.Text);
            }
        }

        private void textBoxMaterialQuantity_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (int.TryParse(materialQuantityTextBox.Text, out materialQuantity))
            {

            }
            else
            {
                materialQuantityTextBox.Text = "0";
            }
        }

        private void buttonAddMaterial_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MaterialTransactionScript.AddMaterialToTheProcessOperation(deviceModelId, processId, operationId, materialId, materialQuantity);
                statusTextBlock.Text = "Успешно";
                statusTextBlock.Foreground = new SolidColorBrush(Colors.Green);

            }
            catch (Exception ex)
            {
                statusTextBlock.Text = ex.Message;
                statusTextBlock.Foreground = new SolidColorBrush(Colors.Red);
            }
        }
    }
}
