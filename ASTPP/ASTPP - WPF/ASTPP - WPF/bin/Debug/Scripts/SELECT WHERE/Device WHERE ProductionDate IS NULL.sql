SELECT [dbo].[Device].[DeviceID]
	  ,[dbo].[DeviceModel].[ModelName]
      ,[dbo].[DeviceType].[DeviceTypeName]
      ,[dbo].[Device].[SerialNumber]
  FROM [dbo].[Device]
  JOIN [dbo].[DeviceModel]
	ON [dbo].[Device].[DeviceModelID]=[dbo].[DeviceModel].[DeviceModelID]
  JOIN [dbo].[DeviceType]
	ON [dbo].[Device].[DeviceTypeID]=[dbo].[DeviceType].[DeviceTypeID]
  	WHERE [dbo].[Device].[ProductionDate] IS NULL


