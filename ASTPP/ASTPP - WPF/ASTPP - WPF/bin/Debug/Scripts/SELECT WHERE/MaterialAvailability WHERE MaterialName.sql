SELECT [dbo].[MaterialAvailability].[MaterialAvailabilityID]
		  ,[dbo].[Material].[MaterialName]
		  ,[dbo].[MaterialAvailability].[CheckDate]
		  ,[dbo].[MaterialAvailability].[Quantity]
		  FROM [dbo].[MaterialAvailability]
		  JOIN [dbo].[Material]
		  ON [dbo].[MaterialAvailability].[MaterialID]=[dbo].[Material].[MaterialID]
		  WHERE [MaterialName]=