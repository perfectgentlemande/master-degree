SELECT [dbo].[Material].[MaterialID]
      ,[dbo].[Material].[MaterialName]
	  ,[dbo].[Material].[MaterialMesure]
	  ,[dbo].[MaterialType].[MaterialTypeName]
  FROM [dbo].[Material]
  JOIN [dbo].[MaterialType]
	ON [dbo].[Material].[MaterialTypeID]=[dbo].[MaterialType].[MaterialTypeID]
	WHERE [dbo].[MaterialType].[MaterialTypeName]=