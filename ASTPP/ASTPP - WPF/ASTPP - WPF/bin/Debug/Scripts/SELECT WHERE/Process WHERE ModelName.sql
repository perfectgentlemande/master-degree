SELECT DISTINCT 
       [dbo].[Process].[ProcessID]  
	 , [dbo].[Process].[ProcessName]  
     , [dbo].[DeviceModel].[ModelName]  
  FROM [dbo].[OperationProcess]
  JOIN [dbo].[Process]
	ON [dbo].[OperationProcess].[ProcessID]=[dbo].[Process].[ProcessID]
  JOIN [dbo].[DeviceModel]
	ON [dbo].[OperationProcess].[DeviceModelID]=[dbo].[DeviceModel].[DeviceModelID]
  	WHERE [dbo].[DeviceModel].[ModelName]=