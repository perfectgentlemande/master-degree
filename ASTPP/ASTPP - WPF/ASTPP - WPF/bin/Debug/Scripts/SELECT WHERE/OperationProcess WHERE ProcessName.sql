SELECT [OperationNumberInProcess]
	  ,[dbo].[Operation].[OperationName]
      ,[dbo].[Process].[ProcessName]
      ,[dbo].[DeviceModel].[ModelName]
      ,[dbo].[DeviceType].[DeviceTypeName]
      ,[Decription]
      ,[Duration]
  FROM [dbo].[OperationProcess]
  JOIN [dbo].[Operation]
	ON [dbo].[OperationProcess].[OperationID]=[dbo].[Operation].[OperationID]
  JOIN [dbo].[Process]
	ON [dbo].[OperationProcess].[ProcessID]=[dbo].[Process].[ProcessID]
  JOIN [dbo].[DeviceModel]
	ON [dbo].[OperationProcess].[DeviceModelID]=[dbo].[DeviceModel].[DeviceModelID]
  JOIN [dbo].[DeviceType]
  	ON [dbo].[OperationProcess].[DeviceTypeID]=[dbo].[DeviceType].[DeviceTypeID]
	WHERE [ProcessName]=


