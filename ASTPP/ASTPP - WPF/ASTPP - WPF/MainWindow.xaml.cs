﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ASTPP___WPF.Control;

namespace ASTPP___WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            InitializeStartScreen();
        }

        //Инициализация экранов
        public void InitializeStartScreen()
        {
            var currentDate = DateTime.Now;
            textBlockCurrentDate.Text += String.Format("{0:dddd}", currentDate)+", "+String.Format("{0:D}", currentDate);
            tabControlMain.Visibility = Visibility.Collapsed;
        }
        public void InitializeWorkingScreen()
        {
            buttonStart.Visibility = Visibility.Collapsed;
            tabControlMain.Visibility = Visibility.Visible;
        }
        //Загрузка вкладок
        public void LoadDeviceTab()
        {
            labelWelcome.Content = "Приборы";
            dataGridDevice.ItemsSource = DeviceTransactionScript.ShowAll();
            dataGridDeviceAdditional.ItemsSource = null;
            labelDeviceAdditional.Visibility = Visibility.Visible;
            textBlockDeviceAdditional.Visibility = Visibility.Visible;
        }
        public void LoadMaterialTab()
        {
            labelWelcome.Content = "Материалы";
            dataGridMaterial.ItemsSource = MaterialTransactionScript.ShowAll();
            dataGridMaterialAdditional.ItemsSource = null;
            labelMaterialAdditional.Visibility = Visibility.Visible;
            textBlockMaterialAdditional.Visibility = Visibility.Visible;
        }
        public void LoadOperationTab()
        {
            labelWelcome.Content = "Операции";
            dataGridOperation.ItemsSource = OperationTransactionScript.ShowAll();
            dataGridOperationAdditional.ItemsSource = null;
            labelOperationAdditional.Visibility = Visibility.Visible;
            textBlockOperationAdditional.Visibility = Visibility.Visible;
        }
        public void LoadPersonTab()
        {
            labelWelcome.Content = "Персонал";
            dataGridPerson.ItemsSource = PersonTransactionScript.ShowAll();
            dataGridPersonAdditional.ItemsSource = null;
            labelPersonAdditional.Visibility = Visibility.Visible;
            textBlockPersonAdditional.Visibility = Visibility.Visible;
        }
        public void LoadProcessTab()
        {
            labelWelcome.Content = "Технологические процессы";
            dataGridProcess.ItemsSource = ProcessTransactionScript.ShowAll();
            dataGridProcessAdditional.ItemsSource = null;
            labelProcessAdditional.Visibility = Visibility.Visible;
            textBlockProcessAdditional.Visibility = Visibility.Visible;
        }
   
        //События
        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            InitializeWorkingScreen();
        }

        //Переключение вкладок
        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                if (tabItemDevice.IsSelected)
                {
                    LoadDeviceTab();
                }
                if (tabItemMaterial.IsSelected)
                {
                    LoadMaterialTab();
                }
                if (tabItemOperation.IsSelected)
                {
                    LoadOperationTab();
                }
                if (tabItemPerson.IsSelected)
                {
                    LoadPersonTab();
                }
                if (tabItemProcess.IsSelected)
                {
                    LoadProcessTab();
                }
            }      
        }

        //Выбор дополнительной информации
        private void dataGridDevice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBoxItemFinishedDevice.IsSelected)
            {
                dataGridDeviceAdditional.ItemsSource = DeviceTransactionScript.ShowFinishedDevice();
                labelDeviceAdditional.Visibility = Visibility.Collapsed;
                textBlockDeviceAdditional.Visibility = Visibility.Collapsed;
            }
            if (comboBoxItemUnfinishedDevice.IsSelected)
            {
                dataGridDeviceAdditional.ItemsSource = DeviceTransactionScript.ShowUnfinishedDevice();
                labelDeviceAdditional.Visibility = Visibility.Collapsed;
                textBlockDeviceAdditional.Visibility = Visibility.Collapsed;
            }
            if (dataGridDevice.SelectedItems.Count == 1 && comboBoxItemSelectedDevice.IsSelected)
            {
                TextBlock content = dataGridDevice.Columns[0].GetCellContent(dataGridDevice.SelectedItem) as TextBlock;
                if (content != null)
                {
                    dataGridDeviceAdditional.ItemsSource = DeviceTransactionScript.ShowSelectedDeviceByModelName(content.Text);
                    labelDeviceAdditional.Visibility = Visibility.Collapsed;
                    textBlockDeviceAdditional.Visibility = Visibility.Collapsed;
                }
            }
            if (dataGridDevice.SelectedItems.Count == 1 && comboBoxItemSelectedFinishedDevice.IsSelected)
            {
                TextBlock content = dataGridDevice.Columns[0].GetCellContent(dataGridDevice.SelectedItem) as TextBlock;
                if (content != null)
                {
                    dataGridDeviceAdditional.ItemsSource = DeviceTransactionScript.ShowSelectedFinishedDeviceByModelName(content.Text);
                    labelDeviceAdditional.Visibility = Visibility.Collapsed;
                    textBlockDeviceAdditional.Visibility = Visibility.Collapsed;
                }
            }

            if (dataGridDevice.SelectedItems.Count == 1 && comboBoxItemSelectedUnfinishedDevice.IsSelected)
            {
                TextBlock content = dataGridDevice.Columns[0].GetCellContent(dataGridDevice.SelectedItem) as TextBlock;
                if (content != null)
                {
                    dataGridDeviceAdditional.ItemsSource = DeviceTransactionScript.ShowSelectedUnfinishedDeviceByModelName(content.Text);
                    labelDeviceAdditional.Visibility = Visibility.Collapsed;
                    textBlockDeviceAdditional.Visibility = Visibility.Collapsed;
                }
            }




            if (dataGridDevice.SelectedItems.Count == 1 && comboBoxItemSelectedDeviceProcess.IsSelected)
            {
                TextBlock content = dataGridDevice.Columns[0].GetCellContent(dataGridDevice.SelectedItem) as TextBlock;
                if (content != null)
                {
                    dataGridDeviceAdditional.ItemsSource = DeviceTransactionScript.ShowSelectedProcessByDeviceName(content.Text);
                    labelDeviceAdditional.Visibility = Visibility.Collapsed;
                    textBlockDeviceAdditional.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void dataGridMaterial_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            if (dataGridMaterial.SelectedItems.Count == 1 && comboBoxItemSelectedMaterialAvailability.IsSelected)
            {

                TextBlock content = dataGridMaterial.Columns[1].GetCellContent(dataGridMaterial.SelectedItem) as TextBlock;
                if (content != null)
                {
                    dataGridMaterialAdditional.ItemsSource = MaterialTransactionScript.ShowSelectedMaterialAvailability(content.Text);
                    labelMaterialAdditional.Visibility = Visibility.Collapsed;
                    textBlockMaterialAdditional.Visibility = Visibility.Collapsed;
                }
            }
        }
      
        private void dataGridPerson_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dataGridPerson.SelectedItems.Count == 1 && comboBoxItemSelectedPersonOperation.IsSelected)
            {
                TextBlock content = dataGridPerson.Columns[0].GetCellContent(dataGridPerson.SelectedItem) as TextBlock;
                if (content != null)
                {
                    dataGridPersonAdditional.ItemsSource = PersonTransactionScript.ShowOperationBySelectedPersonID(Convert.ToInt32(content.Text));
                    labelPersonAdditional.Visibility = Visibility.Collapsed;
                    textBlockPersonAdditional.Visibility = Visibility.Collapsed;
                }
            }
            if (dataGridPerson.SelectedItems.Count == 1 && comboBoxItemSelectedPersonFinishedOperation.IsSelected)
            {
                TextBlock content = dataGridPerson.Columns[0].GetCellContent(dataGridPerson.SelectedItem) as TextBlock;
                if (content != null)
                {
                    dataGridPersonAdditional.ItemsSource = PersonTransactionScript.ShowFinishedOperationBySelectedPersonID(Convert.ToInt32(content.Text));
                    labelPersonAdditional.Visibility = Visibility.Collapsed;
                    textBlockPersonAdditional.Visibility = Visibility.Collapsed;
                }
            }
            if (dataGridPerson.SelectedItems.Count == 1 && comboBoxItemSelectedPersonUnfinishedOperation.IsSelected)
            {
                TextBlock content = dataGridPerson.Columns[0].GetCellContent(dataGridPerson.SelectedItem) as TextBlock;
                if (content != null)
                {
                    dataGridPersonAdditional.ItemsSource = PersonTransactionScript.ShowUnfinishedOperationBySelectedPersonID(Convert.ToInt32(content.Text));
                    labelPersonAdditional.Visibility = Visibility.Collapsed;
                    textBlockPersonAdditional.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void dataGridProcess_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dataGridProcess.SelectedItems.Count == 1 && comboBoxItemDevicesByProcess.IsSelected)
            {
                TextBlock content = dataGridProcess.Columns[1].GetCellContent(dataGridProcess.SelectedItem) as TextBlock;
                if (content != null)
                {
                    dataGridProcessAdditional.ItemsSource = ProcessTransactionScript.ShowDevicesBySelectedProcess(content.Text);
                    labelProcessAdditional.Visibility = Visibility.Collapsed;
                    textBlockProcessAdditional.Visibility = Visibility.Collapsed;
                }
            }
            if (dataGridProcess.SelectedItems.Count == 1 && comboBoxItemOperationsInProcess.IsSelected)
            {
                TextBlock content = dataGridProcess.Columns[1].GetCellContent(dataGridProcess.SelectedItem) as TextBlock;
                if (content != null)
                {
                    dataGridProcessAdditional.ItemsSource = ProcessTransactionScript.ShowOperationsInSelectedProcess(content.Text);
                    labelProcessAdditional.Visibility = Visibility.Collapsed;
                    textBlockProcessAdditional.Visibility = Visibility.Collapsed;
                }
            }
        }
        
        //Фильтрация
        private void comboBoxFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBoxItemFilterByParts.IsSelected)
            {
                TextBlock content = comboBoxItemFilterByParts.Content as TextBlock;
                dataGridMaterial.ItemsSource = MaterialTransactionScript.ShowMaterialBySelectedMaterialType(content.Text);
            }
            if (comboBoxItemFilterByEquipment.IsSelected)
            {
                TextBlock content = comboBoxItemFilterByEquipment.Content as TextBlock;
                dataGridMaterial.ItemsSource = MaterialTransactionScript.ShowMaterialBySelectedMaterialType(content.Text);
            }
            if (comboBoxItemFilterByTools.IsSelected)
            {
                TextBlock content = comboBoxItemFilterByTools.Content as TextBlock;
                dataGridMaterial.ItemsSource = MaterialTransactionScript.ShowMaterialBySelectedMaterialType(content.Text);
            }
            if (comboBoxItemFilterByConsumables.IsSelected)
            {
                TextBlock content = comboBoxItemFilterByConsumables.Content as TextBlock;
                dataGridMaterial.ItemsSource = MaterialTransactionScript.ShowMaterialBySelectedMaterialType(content.Text);
            }
        }

        private void buttonAddMaterial_Click(object sender, RoutedEventArgs e)
        {
            AddMaterialWindow addMaterialWindow = new AddMaterialWindow();
            addMaterialWindow.Show();
        }
    }
}
