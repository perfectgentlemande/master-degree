﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace ASTPP___WPF.Model
{
    class MaterialTypeGateway
    {
        static MaterialType Find(int id)
        {
            return new MaterialType();
        }
        public static List<MaterialType> Read()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\MaterialType.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<MaterialType> recordSet = new List<MaterialType>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new MaterialType()
                        {
                            MaterialTypeID = reader.GetInt32(0),
                            MaterialTypeName = Convert.ToString(reader.GetValue(1))
                        });
                    }
                }
                return recordSet;
            }
        }
    }
    class MaterialType
    {
        public int MaterialTypeID { get; set; }
        public string MaterialTypeName { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1}", MaterialTypeID, MaterialTypeName);
        }
    }
}
