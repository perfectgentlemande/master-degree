﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace ASTPP___WPF.Model
{
    class PositionGateway
    {
        static Position Find(int id)
        {
            return new Position();
        }
        public static List<Position> Read()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\Position.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<Position> recordSet = new List<Position>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new Position()
                        {
                            PositionID = reader.GetInt32(0),
                            PositionName = Convert.ToString(reader.GetValue(1)),
                            Specialization = Convert.ToString(reader.GetValue(2)),
                            Qualification = Convert.ToString(reader.GetValue(3))
                        });
                    }
                }
                return recordSet;
            }
        }
    }
    class Position
    {
        public int PositionID { get; set; }
        public string PositionName { get; set; }
        public string Specialization { get; set; }
        public string Qualification { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", PositionID, PositionName, Specialization, Qualification);
        }
    }
}
