﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace ASTPP___WPF.Model
{
    class OperationGateway
    {
        static Operation Find(int id)
        {
            return new Operation();
        }
        public static List<Operation> Read()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\Operation.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<Operation> recordSet = new List<Operation>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new Operation()
                        {
                            OperationID = reader.GetInt32(0),
                            OperationName = Convert.ToString(reader.GetValue(1))
                        });
                    }
                }
                return recordSet;
            }
        }
    }
    class Operation
    {
        public int OperationID { get; set; }
        public string OperationName { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1}", OperationID, OperationName);
        }
    }
}
