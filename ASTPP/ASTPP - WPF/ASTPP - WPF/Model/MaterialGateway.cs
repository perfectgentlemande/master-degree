﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace ASTPP___WPF.Model
{
    class MaterialGateway
    {
        static Material Find(int id)
        {
            return new Material();
        }
        public static List<object> Read()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\Material.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            MaterialID = reader.GetInt32(0),
                            MaterialName = Convert.ToString(reader.GetValue(1)),
                            MaterialMeasure = Convert.ToString(reader.GetValue(2)),
                            MaterialTypeName = Convert.ToString(reader.GetValue(3))
                        });
                    }
                }
                return recordSet;
            }
        }
        public static List<object> FindMaterialByMaterialType(string materialTypeName)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\Material WHERE MaterialTypeName.sql") + "'" + materialTypeName + "'";
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            MaterialID = reader.GetInt32(0),
                            MaterialName = Convert.ToString(reader.GetValue(1)),
                            MaterialMeasure = Convert.ToString(reader.GetValue(2)),
                            MaterialTypeName = Convert.ToString(reader.GetValue(3))
                        });
                    }
                }
                return recordSet;
            }
        }
        public static int findMaterialTypeIDByMaterialID(int materialId)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\MaterialTypeID WHERE MaterialID.sql") + Convert.ToString(materialId);
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                int materialTypeId = 0;

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        materialTypeId = reader.GetInt32(0);
                    }
                }
                return materialTypeId;
            }
        }
    }
    class Material
    {
        public int MaterialID { get; set; }
        public int MaterialTypeID { get; set; }
        public string MaterialName { get; set; }
        public string MaterialMeasure { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", MaterialID, MaterialTypeID, MaterialName, MaterialMeasure);
        }
    }
}
