﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace ASTPP___WPF.Model
{
    class DeviceGateway
    {
        public static List<Device> Read()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\Device.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<Device> recordSet = new List<Device>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new Device()
                        {
                            DeviceID = reader.GetInt32(0),
                            DeviceModelID = reader.GetInt32(1),
                            DeviceTypeID = reader.GetInt32(2),
                            SerialNumber = Convert.ToString(reader.GetValue(3)),
                            ProductionDate = Convert.ToDateTime(reader.GetValue(4)),
                        });
                    }
                }
                return recordSet;
            }
        }
        public static List<object> FindFinishedDevice()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\Device WHERE ProductionDate IS NOT NULL.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            DeviceID = reader.GetInt32(0),
                            ModelName = Convert.ToString(reader.GetValue(1)),
                            DeviceTypeName = Convert.ToString(reader.GetValue(2)),
                            SerialNumber = Convert.ToString(reader.GetValue(3)),
                            ProductionDate = Convert.ToString(reader.GetValue(4)),
                        });
                    }
                }
                return recordSet;
            }
        }
        public static List<object> FindUnfinishedDevice()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\Device WHERE ProductionDate IS NULL.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            DeviceID = reader.GetInt32(0),
                            ModelName = Convert.ToString(reader.GetValue(1)),
                            DeviceTypeName = Convert.ToString(reader.GetValue(2)),
                            SerialNumber = Convert.ToString(reader.GetValue(3)),
                        });
                    }
                }
                return recordSet;
            }
        }
        public static List<object> FindDeviceByModelName(string modelName)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\Device WHERE ModelName.sql") + "'" + modelName + "'";
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            DeviceID = reader.GetInt32(0),
                            ModelName = Convert.ToString(reader.GetValue(1)),
                            DeviceTypeName = Convert.ToString(reader.GetValue(2)),
                            SerialNumber = Convert.ToString(reader.GetValue(3)),
                            ProductionDate = Convert.ToString(reader.GetValue(4)),
                        });
                    }
                }
                return recordSet;
            }

        }
        public static List<object> FindFinishedDeviceByModelName(string modelName)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\Device WHERE ModelName AND ProductionDate IS NOT NULL.sql") + "'" + modelName + "'";
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            DeviceID = reader.GetInt32(0),
                            ModelName = Convert.ToString(reader.GetValue(1)),
                            DeviceTypeName = Convert.ToString(reader.GetValue(2)),
                            SerialNumber = Convert.ToString(reader.GetValue(3)),
                            ProductionDate = Convert.ToString(reader.GetValue(4)),
                        });
                    }
                }
                return recordSet;
            }

        }
        public static List<object> FindUnfinishedDeviceByModelName(string modelName)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\Device WHERE ModelName AND ProductionDate IS NULL.sql") + "'" + modelName + "'";
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            DeviceID = reader.GetInt32(0),
                            ModelName = Convert.ToString(reader.GetValue(1)),
                            DeviceTypeName = Convert.ToString(reader.GetValue(2)),
                            SerialNumber = Convert.ToString(reader.GetValue(3)),
                        });
                    }
                }
                return recordSet;
            }

        }
    }
    class Device
    {
        public int DeviceID { get; set; }
        public int DeviceModelID { get; set; }
        public int DeviceTypeID {get;set;}
        public string SerialNumber { get; set; }
        public DateTime ProductionDate { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4}", DeviceID, DeviceModelID, DeviceTypeID, SerialNumber, ProductionDate);
        }
    }
}
