﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace ASTPP___WPF.Model
{
    class ProcessGateway
    {
        public static Process Find(int id)
        {
            return new Process();
        }
        public static List<Process> Read()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\Process.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<Process> recordSet = new List<Process>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new Process()
                        {
                            ProcessID =reader.GetInt32(0),
                            ProcessName = Convert.ToString(reader.GetValue(1))
                        });
                    }
                }
                return recordSet;
            }
        }
        public static List<Process> FindByProcessName(string processName)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\Process WHERE ProcessName.sql")+processName;
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<Process> recordSet = new List<Process>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new Process()
                        {
                            ProcessID = reader.GetInt32(0),
                            ProcessName = Convert.ToString(reader.GetValue(1))
                        });
                    }
                }
                return recordSet;
            }
        }
    }
    class Process
    {
        public int ProcessID { get; set; }
        public string ProcessName { get; set; }
    
        public override string ToString()
        {
            return string.Format("{0} {1}", ProcessID, ProcessName);
        }
    }
}
