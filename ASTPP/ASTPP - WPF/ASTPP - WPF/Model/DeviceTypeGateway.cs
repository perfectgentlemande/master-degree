﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace ASTPP___WPF.Model
{
    class DeviceTypeGateway
    {
        static DeviceType Find(int id)
        {
            return new DeviceType();
        }
        public static List<DeviceType> Read()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\DeviceType.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<DeviceType> recordSet = new List<DeviceType>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new DeviceType()
                        {
                            DeviceTypeID = reader.GetInt32(0),
                            DeviceTypeName = Convert.ToString(reader.GetValue(1))
                        });
                    }
                }
                return recordSet;
            }
        }
    }
    class DeviceType
    {
        public int DeviceTypeID { get; set; }
        public string DeviceTypeName { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1}", DeviceTypeID, DeviceTypeName);
        }
    }
}
