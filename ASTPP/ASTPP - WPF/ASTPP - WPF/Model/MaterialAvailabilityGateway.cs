﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace ASTPP___WPF.Model
{
    class MaterialAvailabilityGateway
    {
        static MaterialAvailability Find(int id)
        {
            return new MaterialAvailability();
        }
        public static List<MaterialAvailability> Read()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\MaterialAvailability.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<MaterialAvailability> recordSet = new List<MaterialAvailability>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new MaterialAvailability()
                        {
                            MaterialAvailabilityID = reader.GetInt32(0),
                            MaterialID = reader.GetInt32(1),
                            MaterialTypeID = reader.GetInt32(2),
                            CheckDate = Convert.ToDateTime(reader.GetValue(3)),
                            Quantity = Convert.ToInt32(reader.GetValue(4))
                        });
                    }
                }
                return recordSet;
            }
        }
        public static List<object> FindMaterialAvailabilityByMaterialName(string materialName)
        {
            using(SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\MaterialAvailability WHERE MaterialName.sql") + "'"+materialName+"'";
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            MaterialAvailabilityID = reader.GetInt32(0),
                            MaterialName = Convert.ToString(reader.GetValue(1)),
                            CheckDate = Convert.ToDateTime(reader.GetValue(2)),
                            Quantity = Convert.ToInt32(reader.GetValue(3))
                        });
                    }
                }
                return recordSet;
            }
        }
    }
    class MaterialAvailability
    {
        public int MaterialAvailabilityID { get; set; }
        public int MaterialID { get; set; }
        public int MaterialTypeID { get; set; }
        public DateTime CheckDate { get; set; }
        public int Quantity { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4}", MaterialAvailabilityID, MaterialID, MaterialTypeID, CheckDate, Quantity);
        }
    }
}
