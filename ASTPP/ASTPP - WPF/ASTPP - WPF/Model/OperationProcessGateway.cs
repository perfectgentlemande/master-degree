﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace ASTPP___WPF.Model
{
    class OperationProcessGateway
    {
        static OperationProcess Find(int id)
        {
            return new OperationProcess();
        }
        public static List<OperationProcess> Read()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\OperationProcess.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<OperationProcess> recordSet = new List<OperationProcess>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new OperationProcess()
                        {
                            OperationID = reader.GetInt32(0),
                            ProcessID = reader.GetInt32(1),
                            DeviceModelID =reader.GetInt32(2),
                            DeviceTypeID =reader.GetInt32(3),
                            Description = Convert.ToString(reader.GetValue(4)),
                            OperationNumberInProcess = Convert.ToInt32(reader.GetValue(5)),
                            Duration = Convert.ToInt32(reader.GetValue(6))
                        });
                    }
                }
                return recordSet;
            }
        }
        public static List<object> FindByProcessName(string processName)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\OperationProcess WHERE ProcessName.sql") +"'"+processName+"'";
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new 
                        {
                            OperationNumberInProcess = Convert.ToInt32(reader.GetValue(0)),
                            OperationName = Convert.ToString(reader.GetValue(1)),
                            ProcessName = Convert.ToString(reader.GetValue(2)),
                            ModelName = Convert.ToString(reader.GetValue(3)),
                            DeviceTypeName = Convert.ToString(reader.GetValue(4)),
                            Decription = Convert.ToString(reader.GetValue(5)),
                            Duration = Convert.ToInt32(reader.GetValue(6))
                        });
                    }
                }
                return recordSet;
            }
        }
        public static List<object> FindDeviceByProcessName(string processName)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\Device WHERE ProcessName.sql") + "'" + processName + "'";
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            ModelName = Convert.ToString(reader.GetValue(0)),
                            DeviceTypeName = Convert.ToString(reader.GetValue(1)),
                        });
                    }
                }
                return recordSet;
            }
        }
        public static List<object> FindProcessByModelName(string modelName)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\Process WHERE ModelName.sql") + "'" + modelName + "'";
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            ProcessID = reader.GetInt32(0),
                            ProcessName = Convert.ToString(reader.GetValue(1)),
                        });
                    }
                }
                return recordSet;
            }
        }
        public static List<object> FindOperationByProcessID(int processId, int deviceModelId)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\OperationProcess WHERE DeviceID AND ProcessID.sql") + "WHERE [dbo].[OperationProcess].[DeviceModelID]=" + Convert.ToString(deviceModelId) + " AND [dbo].[OperationProcess].[ProcessID]=" + Convert.ToString(processId)+ " ORDER BY [dbo].[OperationProcess].[OperationNumberInProcess]";
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            OperationID = reader.GetInt32(0),
                            OperationNumberInProcess = Convert.ToInt32(reader.GetValue(1)),
                            OperationName = Convert.ToString(reader.GetValue(2))
                        });
                    }
                }
                return recordSet;
            }
        }

    }
    class OperationProcess
    {
        public int OperationID { get; set; }
        public int ProcessID { get; set; }
        public int DeviceModelID {get; set;}
        public int DeviceTypeID { get; set; }
        public string Description { get; set; }
        public int OperationNumberInProcess { get; set; }
        public int Duration { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5} {6}", OperationID, ProcessID, DeviceModelID, DeviceTypeID, Description, OperationNumberInProcess, Duration);
        }
    }
}
