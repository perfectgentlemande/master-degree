﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace ASTPP___WPF.Model
{
    class DeviceOperationProcessPersonMaterialGateway
    {
        static DeviceOperationProcessPersonMaterial Find(int id)
        {
            return new DeviceOperationProcessPersonMaterial();
        }
        public static List<DeviceOperationProcessPersonMaterial> Read()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\DeviceOperationProcessPersonMaterial.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<DeviceOperationProcessPersonMaterial> recordSet = new List<DeviceOperationProcessPersonMaterial>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new DeviceOperationProcessPersonMaterial()
                        {
                            ProcessID = reader.GetInt32(0),
                            OperationID = reader.GetInt32(1),
                            PersonID = reader.GetInt32(2),
                            MaterialID = reader.GetInt32(3),
                            MaterialTypeID = reader.GetInt32(4),
                            DeviceID = reader.GetInt32(5),
                            DeviceTypeID = reader.GetInt32(6),
                            PositionID = reader.GetInt32(7),
                            DeviceModelID = reader.GetInt32(8),
                            DateOfMaterialReceiving = Convert.ToDateTime(reader.GetValue(9)),
                            QuantityOfReceivedMaterial = Convert.ToInt32(reader.GetValue(10))
                        });
                    }
                }
                return recordSet;
            }
        }
    }
    class DeviceOperationProcessPersonMaterial
    {
        public int ProcessID { get; set; }
        public int OperationID { get; set; }
        public int PersonID { get; set; }
        public int MaterialID { get; set; }
        public int MaterialTypeID { get; set; }
        public int DeviceID { get; set; }
        public int DeviceTypeID { get; set; }
        public int PositionID { get; set; }
        public int DeviceModelID { get; set; }
        public DateTime DateOfMaterialReceiving { get; set; }
        public int QuantityOfReceivedMaterial { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10}", ProcessID, OperationID, PersonID, MaterialID, MaterialTypeID, DeviceID, DeviceTypeID, PositionID, DeviceModelID, DateOfMaterialReceiving, QuantityOfReceivedMaterial);
        }
    }
}
