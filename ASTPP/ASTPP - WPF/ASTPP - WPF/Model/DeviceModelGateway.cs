﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace ASTPP___WPF.Model
{
    class DeviceModelGateway
    {
        static DeviceModel Find(int id)
        {
            return new DeviceModel();
        }
        public static List<DeviceModel> Read()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\DeviceModel.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<DeviceModel> recordSet = new List<DeviceModel>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new DeviceModel()
                        {
                            DeviceModelID = reader.GetInt32(0),
                            DeviceTypeID = reader.GetInt32(1),
                            ModelName = Convert.ToString(reader.GetValue(2)),
                        });
                    }
                }
                return recordSet;
            }
        }
        public static List<object> FindWithID()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\DeviceModelWithID.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            DeviceModelID = reader.GetInt32(0),
                            ModelName = Convert.ToString(reader.GetValue(1)),
                        });
                    }
                }
                return recordSet;
            }
        }
        public static int findDeviceTypeIDByModelID(int deviceModelId)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\DeviceTypeID WHERE DeviceModelID.sql") + Convert.ToString(deviceModelId);
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                int deviceTypeId=0;

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        deviceTypeId = reader.GetInt32(0);
                    }
                }
                return deviceTypeId;
            }
        }
    }
    class DeviceModel
    {
        public int DeviceModelID { get; set; }
        public int DeviceTypeID { get; set; }
        public string ModelName { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1} {2}", DeviceModelID, DeviceTypeID, ModelName);
        }
    }
}
