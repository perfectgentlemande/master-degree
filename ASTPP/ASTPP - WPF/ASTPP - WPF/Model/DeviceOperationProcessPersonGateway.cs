﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace ASTPP___WPF.Model
{
    class DeviceOperationProcessPersonGateway
    {
        static DeviceOperationProcessPerson Find(int id)
        {
            return new DeviceOperationProcessPerson();
        }
        public static List<DeviceOperationProcessPerson> Read()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\DeviceOperationProcessPerson.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<DeviceOperationProcessPerson> recordSet = new List<DeviceOperationProcessPerson>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new DeviceOperationProcessPerson()
                        {
                            ProcessID = reader.GetInt32(0),
                            OperationID = reader.GetInt32(1),
                            PersonID = reader.GetInt32(2),
                            DeviceID = reader.GetInt32(3),
                            PositionID = reader.GetInt32(4),
                            DeviceModelID = reader.GetInt32(5),
                            DeviceTypeID = reader.GetInt32(6),
                            StartDate = Convert.ToDateTime(reader.GetValue(7)),
                            EndDate = Convert.ToDateTime(reader.GetValue(8)),
                            Status = Convert.ToString(reader.GetValue(9))
                        });
                    }
                }
                return recordSet;
            }
        }
        public static List<object> FindOperationByPersonID(int id)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\DeviceOperationProcessPerson WHERE PersonID.sql") + Convert.ToString(id);
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            PersonID = reader.GetInt32(0),
                            LastName = Convert.ToString(reader.GetValue(1)),
                            FirstName = Convert.ToString(reader.GetValue(2)),
                            ModelName = Convert.ToString(reader.GetValue(3)),
                            SerialNumber = Convert.ToString(reader.GetValue(4)),
                            OperationName = Convert.ToString(reader.GetValue(5)),
                            Status = Convert.ToString(reader.GetValue(6)),
                            StartDate = Convert.ToString(reader.GetValue(7)),
                            EndDate = Convert.ToString(reader.GetValue(8)),
                        });
                    }
                }
                return recordSet;
            }
        }
        public static List<object> FindFinishedOperationByPersonID(int id)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\DeviceOperationProcessPerson WHERE PersonID AND EndDate IS NOT NULL.sql") + Convert.ToString(id);
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            PersonID = reader.GetInt32(0),
                            LastName = Convert.ToString(reader.GetValue(1)),
                            FirstName = Convert.ToString(reader.GetValue(2)),
                            ModelName = Convert.ToString(reader.GetValue(3)),
                            SerialNumber = Convert.ToString(reader.GetValue(4)),
                            OperationName = Convert.ToString(reader.GetValue(5)),
                            Status = Convert.ToString(reader.GetValue(6)),
                            StartDate = Convert.ToString(reader.GetValue(7)),
                            EndDate = Convert.ToString(reader.GetValue(8)),
                        });
                    }
                }
                return recordSet;
            }
        }
        public static List<object> FindUnfinishedOperationByPersonID(int id)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT WHERE\DeviceOperationProcessPerson WHERE PersonID AND EndDate IS NULL.sql") + Convert.ToString(id);
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<object> recordSet = new List<object>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new
                        {
                            PersonID = reader.GetInt32(0),
                            LastName = Convert.ToString(reader.GetValue(1)),
                            FirstName = Convert.ToString(reader.GetValue(2)),
                            ModelName = Convert.ToString(reader.GetValue(3)),
                            SerialNumber = Convert.ToString(reader.GetValue(4)),
                            OperationName = Convert.ToString(reader.GetValue(5)),
                            Status = Convert.ToString(reader.GetValue(6)),
                            StartDate = Convert.ToString(reader.GetValue(7)),
                        });
                    }
                }
                return recordSet;
            }
        }
    }
    class DeviceOperationProcessPerson
    {
        public int ProcessID { get; set; }
        public int OperationID { get; set; }
        public int PersonID { get; set; }
        public int DeviceID { get; set; }
        public int PositionID { get; set; }
        public int DeviceModelID { get; set; }
        public int DeviceTypeID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9}", ProcessID, OperationID, PersonID, DeviceID, PositionID, DeviceModelID, DeviceTypeID, StartDate, EndDate, Status);
        }
    }
}
