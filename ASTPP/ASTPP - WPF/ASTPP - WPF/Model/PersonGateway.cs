﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace ASTPP___WPF.Model
{
    class PersonGateway
    {
        static Person Find(int id)
        {
            return new Person();
        }
        public static List<Person> Read()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\Person.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<Person> recordSet = new List<Person>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new Person()
                        {
                            PersonID = reader.GetInt32(0),
                            PositionID = reader.GetInt32(1),
                            LastName = Convert.ToString(reader.GetValue(2)),
                            FirstName = Convert.ToString(reader.GetValue(3)),  
                            Patronym = Convert.ToString(reader.GetValue(4))
                        });
                    }
                }
                return recordSet;
            }
        }
    }
    class Person
    {
        public int PersonID { get; set; }
        public int PositionID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Patronym { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4}", PersonID, PositionID, LastName, FirstName, Patronym);
        }
    }
}
