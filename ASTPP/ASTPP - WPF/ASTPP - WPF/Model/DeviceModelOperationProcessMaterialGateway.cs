﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace ASTPP___WPF.Model
{
    class DeviceModelOperationProcessMaterialGateway
    {
        static DeviceModelOperationProcessMaterial Find(int id)
        {
            return new DeviceModelOperationProcessMaterial();
        }
        public static List<DeviceModelOperationProcessMaterial> Read()
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = File.ReadAllText(@"Scripts\SELECT\DeviceModelOperationProcessMaterial.sql");
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataReader reader = command.ExecuteReader();

                List<DeviceModelOperationProcessMaterial> recordSet = new List<DeviceModelOperationProcessMaterial>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        recordSet.Add(new DeviceModelOperationProcessMaterial()
                        {
                            ProcessID = reader.GetInt32(0),
                            OperationID = reader.GetInt32(1),
                            MaterialID = reader.GetInt32(2),
                            MaterialTypeID = reader.GetInt32(3),
                            DeviceModelID = reader.GetInt32(4),
                            DeviceTypeID = reader.GetInt32(5),
                            NecessaryQuantity = Convert.ToInt32(reader.GetValue(6)),
                        });
                    }
                }
                return recordSet;
            }
        }
        public static void Insert(int processId, int operationId, int materialId, int materialTypeId, int deviceModelId, int deviceTypelId, int necessaryQuantity)
        {
            using (SqlConnection connection = new SqlConnection("Server=DESKTOP-QVSL2FA;Database=ASTPP;Trusted_Connection=true"))
            {
                connection.Open();

                string script = "INSERT INTO DeviceModelOperationProcessMaterial(ProcessID, OperationID, MaterialID, MaterialTypeID, DeviceModelID, DeviceTypeID, NecessaryQuantity) VALUES(";
                script += Convert.ToString(processId) + ",";
                script += Convert.ToString(operationId) + ",";
                script += Convert.ToString(materialId) + ",";
                script += Convert.ToString(materialTypeId) + ",";
                script += Convert.ToString(deviceModelId) + ",";
                script += Convert.ToString(deviceTypelId) + ",";
                script += Convert.ToString(necessaryQuantity) + ")";
                SqlCommand command = new SqlCommand(script, connection);
                int number = command.ExecuteNonQuery();
            }
        }
    }
    class DeviceModelOperationProcessMaterial
    {
        public int ProcessID { get; set; }
        public int OperationID { get; set; }    
        public int MaterialID { get; set; }
        public int MaterialTypeID { get; set; }
        public int DeviceModelID { get; set; }
        public int DeviceTypeID { get; set; }
        public int NecessaryQuantity { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4} {5} {6}", ProcessID, OperationID, MaterialID, MaterialTypeID, DeviceModelID, DeviceTypeID, NecessaryQuantity);
        }
    }
}
