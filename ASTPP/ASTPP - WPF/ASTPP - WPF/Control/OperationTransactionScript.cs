﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASTPP___WPF.Model;

namespace ASTPP___WPF.Control
{
    static class OperationTransactionScript
    {
        public static List<object> ShowAll()
        {
            List<Operation> operations = OperationGateway.Read();

            List<object> selection = (from operation in operations
                                      select new
                                      {
                                          operation.OperationName
                                      }).ToList<object>();
            return selection;
        }
        public static List<object> ShowOperationsByProcessID(int processId, int deviceModelId)
        {
            return OperationProcessGateway.FindOperationByProcessID(processId, deviceModelId);
        }
    }
}
