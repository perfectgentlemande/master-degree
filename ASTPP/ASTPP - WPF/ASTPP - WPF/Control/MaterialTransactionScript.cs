﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASTPP___WPF.Model;

namespace ASTPP___WPF.Control
{
    static class MaterialTransactionScript
    {
        public static List<object> ShowAll()
        {
            return MaterialGateway.Read();
        }
        public static List<object> ShowMaterialBySelectedMaterialType(string selectedMaterialTypeName)
        {
            return MaterialGateway.FindMaterialByMaterialType(selectedMaterialTypeName);
        }
        public static List<object> ShowSelectedMaterialAvailability(string selectedMaterialName)
        {
            return MaterialAvailabilityGateway.FindMaterialAvailabilityByMaterialName(selectedMaterialName);
        }
        public static void AddMaterialToTheProcessOperation(int deviceModelId, int processId, int operationId, int materialId, int materialQuantity)
        {
            int deviceTypelId = DeviceModelGateway.findDeviceTypeIDByModelID(deviceModelId);
            int materialTypeId = MaterialGateway.findMaterialTypeIDByMaterialID(materialId);

            DeviceModelOperationProcessMaterialGateway.Insert(processId, operationId, materialId, materialTypeId, deviceModelId, deviceTypelId, materialQuantity);
        }
    }
}
