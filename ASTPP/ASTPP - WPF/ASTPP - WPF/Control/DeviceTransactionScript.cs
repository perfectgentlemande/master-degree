﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASTPP___WPF.Model;

namespace ASTPP___WPF.Control
{
    static class DeviceTransactionScript
    {
        public static List<object> ShowAll()
        {
            List<DeviceModel> deviceModels = DeviceModelGateway.Read();
            List<DeviceType> deviceTypes = DeviceTypeGateway.Read();

            List<object> selection = (from deviceModel in deviceModels
                        join deviceType in deviceTypes
                             on deviceModel.DeviceTypeID equals deviceType.DeviceTypeID
                        select new
                        {
                            deviceModel.ModelName,
                            deviceType.DeviceTypeName
                        }).ToList<object>();
            return selection;
        }
        public static List<object> ShowAllDeviceModelsWithID()
        {
            return DeviceModelGateway.FindWithID();
        }
        public static List<object> ShowFinishedDevice()
        {
            return DeviceGateway.FindFinishedDevice();
        }
        public static List<object> ShowUnfinishedDevice()
        {
            return DeviceGateway.FindUnfinishedDevice();
        }
        public static List<object> ShowSelectedDeviceByModelName(string selectedDeviceModelName)
        {
            return DeviceGateway.FindDeviceByModelName(selectedDeviceModelName);
        }
        public static List<object> ShowSelectedFinishedDeviceByModelName(string selectedDeviceModelName)
        {
            return DeviceGateway.FindFinishedDeviceByModelName(selectedDeviceModelName);
        }
        public static List<object> ShowSelectedUnfinishedDeviceByModelName(string selectedDeviceModelName)
        {
            return DeviceGateway.FindUnfinishedDeviceByModelName(selectedDeviceModelName);
        }
        public static List<object> ShowSelectedProcessByDeviceName(string selectedDeviceModelName)
        {
            return OperationProcessGateway.FindProcessByModelName(selectedDeviceModelName);
        }
    }
}
