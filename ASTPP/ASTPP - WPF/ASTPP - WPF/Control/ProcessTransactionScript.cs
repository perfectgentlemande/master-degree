﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASTPP___WPF.Model;

namespace ASTPP___WPF.Control
{
    static class ProcessTransactionScript
    {
        public static List<object> ShowAll()
        {
            return ProcessGateway.Read().ToList<object>();
        }
        public static List<object> ShowDevicesBySelectedProcess(string selectedProcessName)
        {
            return OperationProcessGateway.FindDeviceByProcessName(selectedProcessName);
        }
        public static List<object> ShowProcessesBySelectedDevice(string selectedDeviceName)
        {
            return OperationProcessGateway.FindProcessByModelName(selectedDeviceName);
        }
        public static List<object> ShowOperationsInSelectedProcess(string selectedProcessName)
        {
            return OperationProcessGateway.FindByProcessName(selectedProcessName);
        }
    }
}
