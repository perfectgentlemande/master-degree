﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASTPP___WPF.Model;

namespace ASTPP___WPF.Control
{
    static class PersonTransactionScript
    {
        public static List<object> ShowAll()
        {
            List<Person> persons = PersonGateway.Read();
            List<Position> positions = PositionGateway.Read();

            List<object> selection = (from person in persons
                                      join position in positions
                                           on person.PositionID equals position.PositionID
                                      select new
                                      {
                                          person.PersonID,
                                          person.LastName,
                                          person.FirstName,
                                          person.Patronym,
                                          position.PositionName,
                                          position.Specialization,
                                          position.Qualification
                                      }).ToList<object>();
            return selection;
        }
        public static List<object> ShowOperationBySelectedPersonID(int personId)
        {
            return DeviceOperationProcessPersonGateway.FindOperationByPersonID(personId);
        }
        public static List<object> ShowFinishedOperationBySelectedPersonID(int personId)
        {
            return DeviceOperationProcessPersonGateway.FindFinishedOperationByPersonID(personId);
        }
        public static List<object> ShowUnfinishedOperationBySelectedPersonID(int personId)
        {
            return DeviceOperationProcessPersonGateway.FindUnfinishedOperationByPersonID(personId);
        }
    }
}
